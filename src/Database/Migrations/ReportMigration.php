<?php

namespace Safebits\Report\Database\Migrations;

use Illuminate\Database\Migrations\Migration;

/**
 * Class ReportMigration
 * @package Safebits\Report\Database\Migrations
 */
class ReportMigration extends Migration
{
    /**
     * @var string
     */
    protected $connection;

    /**
     * CreateSystemTable constructor.
     */
    public function __construct()
    {
        $this->connection = config('safebits_report.connection');
    }
}
