<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Safebits\Report\Database\Migrations\ReportMigration;

/**
 * Class CreateReportRequestTable
 */
class CreateReportRequestTable extends ReportMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_request', function (Blueprint $table) {
            $table->increments('reportRequestId');
            $table->integer('userId')->unsigned()->nullable()->default(null);
            $table->string('reportTag',100);
            $table->string('fileName',250);
            $table->string('url',500);
            $table->json('parameters');
            $table->boolean('generated');
            $table->boolean('isProcessing');
            $table->boolean('isEmpty')->nullable()->default(null);
            $table->timestamp('requestedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('processBeginAt')->nullable()->default(null);
            $table->timestamp('processEndAt')->nullable()->default(null);
            $table->timestamp('usedAt')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_request');
    }
}
