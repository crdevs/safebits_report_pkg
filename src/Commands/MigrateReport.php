<?php

namespace Safebits\Report\Commands;
use Illuminate\Console\Command;

/**
 * Class MigrateReport
 * @package Safebits\Report\Commands
 */
class MigrateReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sb:migrate-report {--refresh} {--seed}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs Safebits Report migrations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Laravel does not allow full paths when executing migrations
        $fullPath = dirname(__DIR__) . "/Database/Migrations";
        $migrationsPath = str_replace(base_path(), '', $fullPath);

        //Checks if refresh was requested
        if ($this->hasOption('refresh') && $this->option('refresh')) {
            $migrationCommand = 'migrate:refresh';
        } else {
            $migrationCommand = 'migrate';
        }

        //Connection is required in order to create migrations table
        $connection = config('safebits_report.connection');

        \Schema::connection($connection)->disableForeignKeyConstraints();

        //Executes migrations
        \Artisan::call($migrationCommand, array('--path' => $migrationsPath, '--force' => true, '--database' => $connection));

        \Schema::connection($connection)->enableForeignKeyConstraints();
    }
}
