<?php

namespace Safebits\Report;

use Illuminate\Support\ServiceProvider;
use Safebits\Report\Commands\MigrateReport;

/**
 * Class SafebitsReportServiceProvider
 * @package Safebits\Report
 */
class SafebitsReportServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Registers provider commands
        if ($this->app->runningInConsole()) {
            $this->commands([
                MigrateReport::class,
            ]);
        }
        // Publishes config file to local configuration path
        $this->publishes([
            __DIR__ . '/Config/safebits_report.php' => config_path('safebits_report.php'),
        ]);
    }
}
