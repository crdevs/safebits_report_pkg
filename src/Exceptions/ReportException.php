<?php

namespace Safebits\Report\Exceptions;

/**
 * Class ReportException
 * @package Safebits\Report\Exceptions
 */
class ReportException extends \Exception
{
    /**
     * ReportException constructor.
     * @param $message
     * @param $code
     * @param null $previous
     */
    public function __construct($message, $code, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
