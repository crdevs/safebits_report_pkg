<?php

namespace Safebits\Report\Exceptions;

use Safebits\Report\Constants\ReportConstants;

/**
 * Class RequestException
 * @package Safebits\Report\Exceptions
 */
class RequestException extends ReportException
{
    /**
     * RequestException constructor.
     * @param $message
     * @param null $previous
     */
    public function __construct($message, $previous = null)
    {
        $code = ReportConstants::REPORT_REQUEST_EXCEPTION_CODE;
        parent::__construct($message, $code, $previous);
    }
}
