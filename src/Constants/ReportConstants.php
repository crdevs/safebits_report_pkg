<?php

namespace Safebits\Report\Constants;

/**
 * Class ReportConstants
 * @package Safebits\Report\Constants
 */
abstract class ReportConstants
{
    /**
     *
     */
    const REPORT_REQUEST_EXCEPTION_CODE = 1;
}
