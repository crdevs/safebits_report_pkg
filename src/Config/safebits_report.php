<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections you wish
    | to use as your default connection for the command migrations, seeders,
    | models and so on.
    |
    */

    'connection' => env('DB_CONNECTION_REPORT', 'mysql'),

];
