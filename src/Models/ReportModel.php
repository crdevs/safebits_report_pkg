<?php

namespace Safebits\Report\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Safebits\Report\Models\ReportModel
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Report\Models\ReportModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Report\Models\ReportModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Report\Models\ReportModel query()
 * @mixin \Eloquent
 */
class ReportModel extends Model
{
    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'createdAt';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'updatedAt';

    /**
     * Connection to database
     * @var $connection
     */
    protected $connection;

    /**
     * CMDModel constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->connection = config('safebits_report.connection', 'mysql');
    }
}
