<?php

namespace Safebits\Report\Models;

/**
 * Safebits\Report\Models\ReportRequest
 *
 * @property int $reportRequestId
 * @property int|null $userId
 * @property string $reportTag
 * @property string $fileName
 * @property string $url
 * @property mixed $parameters
 * @property int $generated
 * @property int $isProcessing
 * @property int|null $isEmpty
 * @property string $requestedAt
 * @property string|null $processBeginAt
 * @property string|null $processEndAt
 * @property string|null $usedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Report\Models\ReportRequest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Report\Models\ReportRequest newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Report\Models\ReportRequest query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Report\Models\ReportRequest whereFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Report\Models\ReportRequest whereGenerated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Report\Models\ReportRequest whereIsEmpty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Report\Models\ReportRequest whereIsProcessing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Report\Models\ReportRequest whereParameters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Report\Models\ReportRequest whereProcessBeginAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Report\Models\ReportRequest whereProcessEndAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Report\Models\ReportRequest whereReportRequestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Report\Models\ReportRequest whereReportTag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Report\Models\ReportRequest whereRequestedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Report\Models\ReportRequest whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Report\Models\ReportRequest whereUsedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Report\Models\ReportRequest whereUserId($value)
 * @mixin \Eloquent
 */
class ReportRequest extends ReportModel
{
    /**
     * @var string
     */
    protected $primaryKey = 'reportRequestId';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    public $dates = ['requestedAt', 'processBeginAt', 'processEndAt', 'usedAt'];

    /**
     * Command constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable('report_request');
    }
}
